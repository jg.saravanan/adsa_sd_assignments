"""
https://leetcode.com/problems/happy-number/
"""


def isHappy(n: int) -> bool:
    if n < 1:
        return False

    numstr = str(n)
    arr = []

    while True:
        if int(numstr) == 1:
            return True
        elif (len(numstr) == 1) and (int(numstr) != 7):
            return False

        for i in range(len(numstr)):
            arr.append(int(numstr[i]))
            arr[i] = arr[i] ** 2

        numstr = str(sum(arr))
        arr = []


"""
https://leetcode.com/problems/power-of-two/
"""


def isPowerOfTwo(self, n: int) -> bool:
    import math
    if n > 0:
        x = math.log(n, 2)
        if 2 ** int(x) == n:
            return True
    return False


"""
https://leetcode.com/problems/valid-anagram/
"""


def isAnagram(s: str, t: str) -> bool:
    count_dict = {}
    for c in s:
        if c not in count_dict:
            count_dict[c] = 0
        count_dict[c] += 1

    for c in t:
        if c not in count_dict:
            return False
        count_dict[c] -= 1
        if count_dict[c] == 0:
            del count_dict[c]
    return True if len(count_dict) == 0 else False


"""
https://leetcode.com/problems/ugly-number/
"""


def isUgly(n: int) -> bool:
    if n > 0:
        for i in (2, 3, 5):
            while n % i == 0:
                n = n // i
        if n == 1:
            return True
    return False


"""
https://leetcode.com/problems/move-zeroes/
"""


def moveZeroes(nums: list[int]) -> None:
    """
    Do not return anything, modify nums in-place instead.
    """
    pos = 0
    for i in range(len(nums)):
        el = nums[i]
        if el != 0:
            nums[pos], nums[i] = nums[i], nums[pos]
            pos += 1


"""
https://leetcode.com/problems/reverse-vowels-of-a-string/
"""


def reverseVowels(s: str) -> str:
    s = list(s)
    i = 0
    j = len(s) - 1
    while i < j:
        if not isVowel(s[i]):
            i += 1
        elif not isVowel(s[j]):
            j -= 1
        else:
            s[i], s[j] = s[j], s[i]
            i += 1
            j -= 1

    return ''.join(s)


def isVowel(self, c: str) -> bool:
    if c.lower() in ('a', 'e', 'i', 'o', 'u'):
        return True
    return False


"""
https://leetcode.com/problems/third-maximum-number/
"""


def thirdMax(nums: list[int]) -> int:
    # negative infinity
    m1, m2, m3 = float('-inf'), float('-inf'), float('-inf')
    # print(f"m1:{m1} m2:{m2} m3:{m3}")
    # remove duplicates
    data = set(nums)
    for n in data:
        print(n)
        if m1 < n:
            m3 = m2
            m2 = m1
            m1 = n
        elif m2 < n:
            m3 = m2
            m2 = n
        elif m3 < n:
            m3 = n
    print(f"m1:{m1} m2:{m2} m3:{m3}")
    if m3 == float('-inf'):
        return m1
    else:
        return m3


"""
https://leetcode.com/problems/find-the-difference
"""


def findTheDifference(s: str, t: str) -> str:
    l1 = list(s)
    l2 = list(t)
    for i in l1:
        if i in l2:
            l2.remove(i)
    return ''.join(l2)


"""
https://leetcode.com/problems/add-digits/
"""


def addDigits(num: int) -> int:
    if not num // 10:
        return num
    sum = 0
    while num // 10:
        sum += num % 10
        num //= 10
    sum += num
    return addDigits(sum)


"""
https://leetcode.com/problems/sum-of-digits-of-string-after-convert/
"""


def getLucky(s: str, k: int) -> int:
    s = ''.join([str(ord(c) - 96) for c in s])
    v = int(s)
    for t in range(k):
        v = sum_of_digits(v)
    return v


def sum_of_digits(s: int) -> int:
    sum = 0
    while s // 10:
        sum += s % 10
        s = s // 10
    sum += s
    return sum
