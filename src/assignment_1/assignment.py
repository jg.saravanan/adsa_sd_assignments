"""
Q1. Given an array of N integers. Your task is to print the sum of all of the integers.
Example 1:
Input:
4
1 2 3 4
Output:
10
Example 2:
Input:
6
5 8 3 10 22 45
Output:
93
"""
from operator import index


def sum_of_integers(size: int, data: list) -> None:
    sum = 0
    if size > 0:
        for i in data:
            sum = sum + i
    else:
        return sum
    print(sum)


"""
Q2. Given an array A[] of N integers and an index Key. Your task is to print the element present at
index key in the array.
Example 1:
Input:
5 2
10 20 30 40 50
Output:
30
Example 2:
Input:
7 4
10 20 30 40 50 60 70
Output:
50
"""


def get_value_by_index(data: list, size: int, index: int) -> None:
    if index >= 0 and index < size:
        print(data[index])
    else:
        raise Exception("Invalid Index")


"""
Q3. Given an sorted array A of size N. Find number of elements which are less than or equal to given
element X.
Example 1:
Input:
N = 6
A[] = {1, 2, 4, 5, 8, 10}
X = 9
Output:
5
Example 2:
Input:
N = 7
A[] = {1, 2, 2, 2, 5, 7, 9}
X = 2
Output:
4
"""


def get_elements_lt_by_value(size: int, data: list, value: int) -> int:
    for i in range(size - 1, -1, -1):
        if value >= data[i]:
            print(i + 1)
            break


"""
Q4. You are given an array A of size N. You need to print elements of A in alternate order (starting
from index 0).
Example 1:
Input:
N = 4
A[] = {1, 2, 3, 4}
Output:
1 3
Example 2:
Input:
N = 5
A[] = {1, 2, 3, 4, 5}
Output:
1 3 5
"""


def print_no_alternate_order(size: int, data: list) -> None:
    for i in range(0, size, 2):
        print(data[i], end=' ')


"""
Q5. Given an array Arr of N positive integers. Your task is to find the elements whose value is equal
to that of its index value ( Consider 1-based indexing ).
Example 1:
Input:
N = 5
Arr[] = {15, 2, 45, 12, 7}
Output: 2
Explanation: Only Arr[2] = 2 exists here.
Example 2:
Input:
N = 1
Arr[] = {1}
Output: 1
Explanation: Here Arr[1] = 1 exists.
"""


def get_elements_by_idx_eq_value(size: int, data: list) -> None:
    for i in range(size):
        if i + 1 == data[i]:
            print(data[i], end=' ')


"""
Q6. Given an array of size N and you have to tell whether the array is perfect or not. An array is said
to be perfect if it's reverse array matches the original array. If the array is perfect then print
"PERFECT" else print "NOT PERFECT".
Example 1:
Input : Arr[] = {1, 2, 3, 2, 1}
Output : PERFECT
Explanation:
Here we can see we have [1, 2, 3, 2, 1]
if we reverse it we can find [1, 2, 3, 2, 1]
which is the same as before.
So, the answer is PERFECT.
Example 2:
Input : Arr[] = {1, 2, 3, 4, 5}
Output : NOT PERFECT
"""


def is_perfect_array(data: list) -> None:
    size = len(data)
    if size % 2 == 1:
        for i in range(0, size // 2):
            if data[i] != data[size - (i + 1)]:
                print("NOT PERFECT")
                break
            elif i == size // 2 - 1:
                print("PERFECT")
    else:
        print("NOT PERFECT")


"""
Q7. Given an array of length N, at each step it is reduced by 1 element. In the first step the maximum
element would be removed, while in the second step minimum element of the remaining array would
be removed, in the third step again the maximum and so on. Continue this till the array contains only 1
element. And find the final element remaining in the array.
Example 1:
Input:
N = 7
A[] = {7, 8, 3, 4, 2, 9, 5}
Ouput:
5
Explanation:
In first step '9' would be removed, in 2nd step
'2' will be removed, in third step '8' will be
removed and so on. So the last remaining
element would be '5'.
Example 2:
Input:
N = 8
A[] = {8, 1, 2, 9, 4, 3, 7, 5}
Ouput:
4
"""


def print_array_by_logic(size: int, data: list) -> None:
    step = 1
    while size > 1:
        element = 0
        if step % 2 == 1:
            for i in range(1, size):
                if data[element] < data[i]:
                    element = i
            del data[element]
        else:
            for i in range(1, size):
                if data[element] > data[i]:
                    element = i
            del data[element]
        step += 1
        size = len(data)
    print(data[0])


"""
Q8. Given an array of N distinct elements, the task is to find all elements in array except two greatest
elements in sorted order.
Example 1:
Input :
a[] = {2, 8, 7, 1, 5}
Output :
1 2 5
Explanation :
The output three elements have two or
more greater elements.
Example 2:
Input :
a[] = {7, -2, 3, 4, 9, -1}
Output :
-2 -1 3 4
"""


def get_except_x_gr_elements(data: list, x: int = 2) -> list:
    size = len(data)
    for i in range(0, size - x):
        for j in range(i + 1, size):
            if data[i] > data[j]:
                data[i], data[j] = data[j], data[i]
        print(data[i], end=' ')


"""
Q9. Write a program to find the sum of the given series 1+2+3+ . . . . . .(N terms)
Example 1:
Input:
N = 1
Output: 1
Explanation: For n = 1, sum will be 1.
Example 2:
Input:
N = 5
Output: 15
Explanation: For n = 5, sum will be 1
+ 2 + 3 + 4 + 5 = 15.
"""


def sum_series_of_elements(terms: int) -> None:
    print((terms * (terms + 1)) // 2)


"""
Q10. Given a number N. Your task is to check whether it is fascinating or not.
Fascinating Number: When a number(should contain 3 digits or more) is multiplied by 2 and 3 ,and
when both these products are concatenated with the original number, then it results in all digits from 1
to 9 present exactly once.
Example 1:
Input:
N = 192
Output: Fascinating
Explanation: After multiplication with 2
and 3, and concatenating with original
number, number will become 192384576
which contains all digits from 1 to 9.
Example 2:
Input:
N = 853
Output: Not Fascinating
Explanation: It's not a fascinating
number.
"""


def is_fascinating_number(n: int) -> None:
    value = str(n) + str(n * 2) + str(n * 3)
    sum = 9 * 10 // 2
    for i in value:
        sum -= int(i)
    if sum == 0:
        print("FASCINATED")
    else:
        print("NOT FASCINATED")


"""
Bonus Question
Given an array of even size N, task is to find minimum value that can be added to an element so that
array become balanced. An array is balanced if the sum of the left half of the array elements is equal
to the sum of right half.
Example 1:
Input:
N = 4
arr[] = {1, 5, 3, 2}
Output: 1
Explanation:
Sum of first 2 elements is 1 + 5 = 6,
Sum of last 2 elements is 3 + 2 = 5,
To make the array balanced you can add 1.
Example 2:
Input:
N = 6
arr[] = { 1, 2, 1, 2, 1, 3 }
Output: 2
Explanation:
Sum of first 3 elements is 1 + 2 + 1 = 4,
Sum of last three elements is 2 + 1 + 3 = 6,
To make the array balanced you can add 2.
"""


def min_value_for_sum_left_half_right_half(size: int, data: list) -> int:
    if size % 2 == 0:
        a, b = 0, 0
        for i in range(size // 2):
            a += data[i]
            b += data[size - 1 - i]
        if a > b:
            print(a - b)
        elif a < b:
            print(b - a)
        else:
            print(0)
    else:
        raise Exception("Invalid Data")


if __name__ == '__main__':
    # sum_of_integers(4, [1, 2, 3, 4])
    # sum_of_integers(6, [5, 8, 3, 10, 22, 45])
    # get_value_by_index([10, 20, 30, 40, 50], size=5, index=2)
    # get_value_by_index([10, 20, 30, 40, 50, 60, 70], size=7, index=4)
    # get_elements_lt_by_value(size=6, data=[1,2,4,5,8,10], value=9)
    # get_elements_lt_by_value(size=7, data=[1, 2, 2, 2, 5, 7, 9], value=2)
    # print_no_alternate_order(size=4, data=[1, 2, 3, 4])
    # print_no_alternate_order(size=5, data=[1, 2, 3, 4, 5])
    # get_elements_by_idx_eq_value(size=5, data=[15, 2, 45, 12, 7])
    # get_elements_by_idx_eq_value(size=1, data=[1])
    # is_perfect_array(data=[1, 2, 3, 2, 1])
    # is_perfect_array(data=[1, 2, 3, 4, 5])
    # get_except_x_gr_elements(data=[2, 8, 7, 1, 5])
    # get_except_x_gr_elements(data=[7, -2, 3, 4, 9, -1])
    # sum_series_of_elements(terms=1)
    # sum_series_of_elements(terms=5)
    # is_fascinating_number(n=192)
    # is_fascinating_number(n=853)
    # min_value_for_sum_left_half_right_half(size=4, data=[1, 5, 3, 2])
    # min_value_for_sum_left_half_right_half(size=6, data=[1, 2, 1, 2, 1, 3])
    # print_array_by_logic(size=7, data=[7, 8, 3, 4, 2, 9, 5])
    print_array_by_logic(size=8, data=[8,1,2,9,4,3,7,5])
    pass
