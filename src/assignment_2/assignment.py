"""
https://leetcode.com/problems/richest-customer-wealth
"""


def maximumWealth(accounts: list[list[int]]) -> int:
    max = 0
    for i in accounts:
        total = 0
        for j in i:
            total += j
        if max < total:
            max = total
    return max


"""
https://leetcode.com/problems/running-sum-of-1d-array
"""


def runningSum(nums: list[int]) -> list[int]:
    for i in range(len(nums)):
        if i > 0:
            nums[i] += nums[i - 1]
    return nums


"""
https://leetcode.com/problems/jewels-and-stones
"""


def numJewelsInStones(jewels: str, stones: str) -> int:
    count = 0
    for s in stones:
        if s in jewels:
            count += 1
    return count


"""
https://leetcode.com/problems/three-consecutive-odds
"""


def threeConsecutiveOdds(arr: list[int]) -> bool:
    consecutive_odds = 0
    for i in arr:
        if i % 2 == 1:
            consecutive_odds += 1
        elif consecutive_odds >= 0 and consecutive_odds < 3:
            consecutive_odds = 0
    if consecutive_odds >= 3:
        return True
    return False


"""
https://leetcode.com/problems/transpose-matrix
"""


def transpose(matrix: list[list[int]]) -> list[list[int]]:
    m = len(matrix)
    if m > 0:
        n = len(matrix[0])
    tmatrix = []
    for i in range(n):
        temp = []
        for j in range(m):
            temp.append(matrix[j][i])
        tmatrix.append(temp)
    return tmatrix


"""
https://leetcode.com/problems/majority-element
"""


def majorityElement(nums: list[int]) -> int:
    count = 0
    max_num = 0
    num_count_dict = {}
    for num in nums:
        if num_count_dict.get(num) is None:
            num_count_dict[num] = 1
        else:
            num_count_dict[num] = num_count_dict.get(num) + 1
        if count <= num_count_dict.get(num):
            count = num_count_dict.get(num)
            max_num = num
    return max_num


"""
https://leetcode.com/problems/move-zeroes
"""


def moveZeroes(nums: list[int]) -> None:
    """
    Do not return anything, modify nums in-place instead.
    """
    pos = 0
    for i in range(len(nums)):
        el = nums[i]
        if el != 0:
            nums[pos], nums[i] = nums[i], nums[pos]
            pos += 1
    print(nums)


"""
https://leetcode.com/problems/minimum-absolute-differenc
"""


def minimumAbsDifference(arr: list[int]) -> list[list[int]]:
    size = len(arr)
    if (2 <= size and size <= 10 ** 5):
        min_diff = 0
        min_list = []
        for i in range(size):
            if -10 ** 6 <= arr[i] and arr[i] <= 10 ** 6:
                for j in range(i + 1, size):
                    if arr[i] > arr[j]:
                        arr[i], arr[j] = arr[j], arr[i]
                if i >= 1 and i < size:
                    if min_diff == 0 or min_diff == abs(arr[i - 1] - arr[i - 1 + 1]):
                        min_list.append([arr[i - 1], arr[i - 1 + 1]])
                        min_diff = abs(arr[i - 1] - arr[i - 1 + 1])
                    elif min_diff > abs(arr[i - 1] - arr[i - 1 + 1]):
                        min_list = []
                        min_list.append([arr[i - 1], arr[i - 1 + 1]])
                        min_diff = abs(arr[i - 1] - arr[i - 1 + 1])
    return min_list


if __name__ == '__main__':
    moveZeroes([1, 0, 0, 3, 12])
